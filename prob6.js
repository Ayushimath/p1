const developerData = require('./p1');

function cb(acc, currentdata) {

    if ((currentdata.location) in acc) {
        acc[currentdata.location]["salary"] += parseFloat((currentdata.salary).slice(1,));
        acc[currentdata.location]["count"] += 1
        acc[currentdata.location]["average"] += parseFloat((acc[currentdata.location]["salary"] /
            acc[currentdata.location]["count"]).toFixed(3));
    }
    else {
        acc[currentdata.location]={}
        acc[currentdata.location]["salary"] = parseFloat((currentdata.salary).slice(1,));
        acc[currentdata.location]["count"] = 1
        acc[currentdata.location]["average"] = parseFloat((acc[currentdata.location]["salary"] /
            acc[currentdata.location]["count"]).toFixed(3));
    }
    return acc;

}

const result6 = developerData.reduce(cb, {});
console.log(result6);